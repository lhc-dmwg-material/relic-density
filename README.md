Curves contributed by A. Albert and T. DuPree

The ROOT TGraphs are in the _graphs_ directory.

The plots in the lepton coupling scenarios are named according to section 2.3.1 of the 2017 DMWG document.

   *	relic_A1.png
   *	relic_A2.png
   * 	relic_V1.png
   *	relic_V2.png 

The plots not fitting this naming scheme correspond to the legacy scenarios with gq = 1.0, which had already been included in the document before:

   * 	relic_axial_gq1.png
   * 	relic_pseudo_gq1.png
   * 	relic_scalar_gq1.png
   *	relic_vector_gq1.png
